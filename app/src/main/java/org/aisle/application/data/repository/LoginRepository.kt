package org.aisle.application.data.repository

import io.reactivex.Single
import org.aisle.application.data.webservice.RestService
import org.aisle.application.model.request.LoginRequestBody
import org.aisle.application.model.response.login.LoginResponse
import retrofit2.Response
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class LoginRepository @Inject constructor() {

    @Inject
    lateinit var restService: RestService


    fun login(
        loginRequestBody: LoginRequestBody
    ): Single<Response<LoginResponse>> {
        return restService.login(loginRequestBody)
    }



}