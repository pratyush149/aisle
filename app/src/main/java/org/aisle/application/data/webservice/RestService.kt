package org.aisle.application.data.webservice

import io.reactivex.Single
import org.aisle.application.model.request.LoginRequestBody
import org.aisle.application.model.request.VerifyOtpRequestBody
import org.aisle.application.model.response.login.LoginResponse
import org.aisle.application.model.response.main.MainResponse
import org.aisle.application.model.response.otp.VerifyOtpResponse
import retrofit2.Response
import retrofit2.http.*

interface RestService {


    @Headers(
        "Content-Type:application/json",
        "Cookie: __cfduid=df9b865983bd04a5de2cf5017994bbbc71618565720"
    )

    @POST("users/phone_number_login")
    fun login(
        @Body loginRequestBody: LoginRequestBody
    ): Single<Response<LoginResponse>>

    @POST("users/verify_otp")
    fun verifyOtp(
        @Body verifyOtpRequestBody: VerifyOtpRequestBody
    ): Single<Response<VerifyOtpResponse>>

    @GET("users/test_profile_list")
    fun getProfileList(
        @Header("Authorization") token: String
    ): Single<Response<MainResponse>>


}