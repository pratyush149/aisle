package org.aisle.application.di.module

import android.content.Context
import org.aisle.application.data.webservice.RestService
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.aisle.application.utils.Session
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module(includes = [ViewModelModule::class])
class ApplicationModule {

    companion object {
        private const val BASE_URL = "https://testa2.aisle.co/V1/"

        @Singleton
        @Provides
        fun provideRetrofit(): Retrofit {
            val interceptor = HttpLoggingInterceptor()
            interceptor.level = HttpLoggingInterceptor.Level.BODY
            val client =
                OkHttpClient.Builder().addInterceptor(interceptor).readTimeout(40, TimeUnit.SECONDS)
                    .build()
            val gson = GsonBuilder()
                .setLenient()
                .create()

            return Retrofit.Builder().baseUrl(BASE_URL)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(client)
                .build()
        }

        @Singleton
        @Provides
        fun provideRetrofitService(retrofit: Retrofit): RestService {
            return retrofit.create<RestService>(RestService::class.java)
        }

        @Singleton
        @Provides
        fun provideSession(context: Context): Session {
            return Session(context)
        }

    }
}