package org.aisle.application.di.module

import dagger.Module
import dagger.android.ContributesAndroidInjector
import org.aisle.application.ui.views.login.LoginActivity
import org.aisle.application.ui.views.login.VerifyOtpActivity
import org.aisle.application.ui.views.main.MainActivity

@Module
abstract class ActivityBindingModule {
    @ContributesAndroidInjector(modules = [MainFragmentBindingModule::class])
    abstract fun bindMainActivity(): MainActivity

    @ContributesAndroidInjector()
    abstract fun bindLoginActivity(): LoginActivity

    @ContributesAndroidInjector()
    abstract fun bindVerifyOtpActivity(): VerifyOtpActivity
}