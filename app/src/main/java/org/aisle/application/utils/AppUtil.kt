package org.aisle.application.utils

import android.app.Activity
import android.content.Context
import android.graphics.Color
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.TextView
import androidx.core.content.ContextCompat
import com.google.android.material.snackbar.Snackbar
import org.aisle.application.R


class AppUtil {

    companion object {
        fun showSnackBar(view: View, context: Context, title: String) {
            val snackBar =
                Snackbar.make(view, title, Snackbar.LENGTH_LONG)
            val snackBarView = snackBar.view
            snackBarView.setBackgroundColor(ContextCompat.getColor(context, R.color.white))

            val textView =
                snackBarView.findViewById(com.google.android.material.R.id.snackbar_text) as TextView

            textView.setTextColor(Color.RED)
            textView.textSize = 20f
            snackBar.show()
        }

        fun hideKeyboard(context: Context) {
            val inputManager =
                (context as Activity).getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            val focusedView = context.currentFocus
            if (focusedView != null) {
                inputManager.hideSoftInputFromWindow(
                    focusedView.windowToken,
                    InputMethodManager.HIDE_NOT_ALWAYS
                )
            }

        }
    }
}