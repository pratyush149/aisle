package org.aisle.application.ui.views.login

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.lifecycle.Observer
import kotlinx.android.synthetic.main.activity_login.*
import org.aisle.application.R
import org.aisle.application.base.BaseActivity
import org.aisle.application.databinding.ActivityLoginBinding
import org.aisle.application.ui.viewmodel.LoginViewModel
import org.aisle.application.ui.views.main.MainActivity
import org.aisle.application.utils.AppUtil
import org.aisle.application.utils.NetworkUtil
import org.aisle.application.utils.Session
import javax.inject.Inject

class LoginActivity : BaseActivity<LoginViewModel, ActivityLoginBinding>() {

    private lateinit var mContext: Context

    @Inject
    lateinit var session: Session

    override fun layoutRes(): Int {
        return R.layout.activity_login
    }

    override fun getViewModelType(): Class<LoginViewModel> {
        return LoginViewModel::class.java
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding.viewmodel = viewModel
        mContext = this


        initViewModelObservers()

        initClickListeners()

    }

    private fun initClickListeners() {
        btContinue.setOnClickListener {
            if (NetworkUtil.isNetworkAvailable(mContext)) {
                viewModel.validateLogin()
                AppUtil.hideKeyboard(mContext)
            }
        }
    }

    private fun initViewModelObservers() {
        viewModel.getLoginData().observe(this, Observer {
            if (it.status) {
                AppUtil.showSnackBar(clLogin, mContext, "Login Success")
                val intent = Intent(mContext, VerifyOtpActivity::class.java)
                intent.putExtra("number", viewModel.code.get() + viewModel.number.get())
                startActivity(intent)
            }
        })

        viewModel.getErrorData().observe(this, Observer {
            AppUtil.showSnackBar(clLogin, mContext, it)
        })

        viewModel.getValidateLogin().observe(this, Observer {
            if (it) {
                viewModel.login()
            }
        })

        viewModel.getLoadingData().observe(this, Observer {
            if (it) {
                showProgressDialog()
            } else {
                dismissProgressDialog()
            }
        })
    }
}