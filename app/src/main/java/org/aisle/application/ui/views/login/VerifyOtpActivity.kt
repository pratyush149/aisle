package org.aisle.application.ui.views.login

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.CountDownTimer
import android.util.Log
import androidx.lifecycle.Observer
import kotlinx.android.synthetic.main.activity_verify_otp.*
import kotlinx.android.synthetic.main.activity_verify_otp.btContinue
import kotlinx.android.synthetic.main.activity_verify_otp.clLogin
import org.aisle.application.R
import org.aisle.application.base.BaseActivity
import org.aisle.application.databinding.ActivityVerifyOtpBinding
import org.aisle.application.ui.viewmodel.VerifyOtpViewModel
import org.aisle.application.ui.views.main.MainActivity
import org.aisle.application.utils.AppUtil
import org.aisle.application.utils.NetworkUtil
import org.aisle.application.utils.Session
import javax.inject.Inject

class VerifyOtpActivity : BaseActivity<VerifyOtpViewModel, ActivityVerifyOtpBinding>() {

    @Inject
    lateinit var session: Session
    private lateinit var mContext: Context
    private lateinit var number: String
    override fun layoutRes(): Int {
        return R.layout.activity_verify_otp
    }

    override fun getViewModelType(): Class<VerifyOtpViewModel> {
        return VerifyOtpViewModel::class.java
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding.viewmodel = viewModel
        mContext = this
        number = intent?.getStringExtra("number").toString()


        setNumberInText()
        initResendTimer()
        initViewModelObservers()
        setClickListeners()
    }

    private fun initResendTimer() {
        val timer = object : CountDownTimer(15000, 1000) {
            override fun onTick(millisUntilFinished: Long) {
                var second: String
                var timerText = String()
                when (millisUntilFinished) {
                    in 10000..15000 -> {
                        second = (millisUntilFinished / 100).toString().substring(0, 2)
                        timerText = "0 : $second"
                    }
                    in 1000..10000 -> {
                        second = "0" + (millisUntilFinished / 100).toString().substring(0, 1)
                        timerText = "0 : $second"
                    }
                    else -> timerText = getString(R.string.resend)
                }

                tvTimer.text = timerText
            }

            override fun onFinish() {
                Log.e("onFinish", "onFinish: ")
                tvTimer.text = getString(R.string.resend)
            }
        }
        timer.start()
    }

    private fun setNumberInText() {
        tvNumber.text = number
    }

    private fun initViewModelObservers() {
        viewModel.getVerifyOtpData().observe(this, Observer {
            session.setToken(it.token)
            startActivity(Intent(mContext, MainActivity::class.java))
            finishAffinity()
        })

        viewModel.getErrorData().observe(this, Observer {
            AppUtil.showSnackBar(clLogin, mContext, it)
        })

        viewModel.getValidateLogin().observe(this, Observer {
            if (it) {
                viewModel.verifyOtp()
            }
        })

        viewModel.getLoadingData().observe(this, Observer {
            if (it) {
                showProgressDialog()
            } else {
                dismissProgressDialog()
            }
        })
    }

    private fun setClickListeners() {
        btContinue.setOnClickListener {
            if (NetworkUtil.isNetworkAvailable(mContext)) {
                AppUtil.hideKeyboard(mContext)
                viewModel.validateOtp()
            }
        }

        tvTimer.setOnClickListener {
            if (tvTimer.text.equals(getString(R.string.resend))) {
                initResendTimer()
            }
        }
    }
}