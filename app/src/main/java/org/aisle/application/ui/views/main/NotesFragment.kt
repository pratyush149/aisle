package org.aisle.application.ui.views.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.NonNull
import org.aisle.application.R
import org.aisle.application.base.BaseFragment
import org.aisle.application.databinding.FragmentNotesBinding
import org.aisle.application.ui.viewmodel.MainViewModel


class NotesFragment : BaseFragment<MainViewModel>() {
    lateinit var binding: FragmentNotesBinding

    override fun layoutRes(): Int {
        return R.layout.fragment_discover
    }

    override fun getViewModelType(): Class<MainViewModel> {
        return MainViewModel::class.java
    }

    override fun onCreateView(
        @NonNull inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        binding = FragmentNotesBinding.inflate(inflater, container, false)
        //set binding variables here
        //set binding variables here
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.lifecycleOwner = this
        binding.viewmodel = viewModel
    }


}