package org.aisle.application.ui.viewmodel

import androidx.databinding.ObservableField
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableSingleObserver
import io.reactivex.schedulers.Schedulers
import org.aisle.application.data.repository.LoginRepository
import org.aisle.application.model.request.LoginRequestBody
import org.aisle.application.model.response.login.LoginResponse
import retrofit2.Response
import java.net.HttpURLConnection
import javax.inject.Inject


class LoginViewModel @Inject constructor(private val loginRepository: LoginRepository) :
    ViewModel() {

    private var disposable: CompositeDisposable? = CompositeDisposable()
    private var loginResponseData: MutableLiveData<LoginResponse> = MutableLiveData()
    private var repoLoadError: MutableLiveData<String> = MutableLiveData()
    private var validate: MutableLiveData<Boolean> = MutableLiveData()
    private var loading: MutableLiveData<Boolean> = MutableLiveData()

    val code: ObservableField<String> = ObservableField("")
    val number: ObservableField<String> = ObservableField("")

    fun login() {
        loading.value = true
        disposable?.add(
            loginRepository.login(LoginRequestBody("+919876543212"))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(object : DisposableSingleObserver<Response<LoginResponse>>() {
                    override fun onSuccess(response: Response<LoginResponse>?) {
                        loading.value = false
                        if (response != null && response.code() == HttpURLConnection.HTTP_OK) {
                            loginResponseData.value = response.body()
                        } else {
                            repoLoadError.value = response?.message()
                        }
                    }

                    override fun onError(e: Throwable?) {
                        loading.value = false
                        repoLoadError.value = e?.message
                    }
                })
        )
    }

    fun validateLogin() {
        if (code.get()?.isEmpty() == true) {
            validate.value = false
            repoLoadError.value = "Please Enter Valid Code"
            return
        }

        if (number.get()?.isEmpty() == true || number.get()?.length!! < 10) {
            validate.value = false
            repoLoadError.value = "Please Enter Valid Number"
            return
        }

        validate.value = true

    }


    fun getValidateLogin(): LiveData<Boolean> {

        return validate
    }

    fun getLoadingData(): LiveData<Boolean> {

        return loading
    }

    fun getLoginData(): LiveData<LoginResponse> {
        return loginResponseData
    }


    fun getErrorData(): LiveData<String> {
        return repoLoadError
    }


    override fun onCleared() {
        disposable?.dispose()
        super.onCleared()
    }
}