package org.aisle.application.model.request

data class VerifyOtpRequestBody(val number:String, val otp:String) {
}