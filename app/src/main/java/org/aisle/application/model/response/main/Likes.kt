package org.aisle.application.model.response.main

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName
import javax.annotation.Generated


@Generated("jsonschema2pojo")
data class Likes (
    @SerializedName("profiles")
    @Expose
    var profiles: List<Profile__1>? = null,

    @SerializedName("can_see_profile")
    @Expose
    var canSeeProfile: Boolean? = null,

    @SerializedName("likes_received_count")
    @Expose
    var likesReceivedCount: Int? = null
)