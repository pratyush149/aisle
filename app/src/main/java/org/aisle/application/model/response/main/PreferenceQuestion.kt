package org.aisle.application.model.response.main

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName
import javax.annotation.Generated


@Generated("jsonschema2pojo")
data class PreferenceQuestion(
    @SerializedName("first_choice")
    @Expose
    var firstChoice: String? = null,

    @SerializedName("second_choice")
    @Expose
    var secondChoice: String? = null
)