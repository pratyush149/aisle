package org.aisle.application.model.response.main

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName
import javax.annotation.Generated


@Generated("jsonschema2pojo")
data class Preference (
    @SerializedName("answer_id")
    @Expose
    var answerId: Int? = null,

    @SerializedName("id")
    @Expose
    var id: Int? = null,

    @SerializedName("value")
    @Expose
    var value: Int? = null,

    @SerializedName("preference_question")
    @Expose
    var preferenceQuestion: PreferenceQuestion? = null
    )