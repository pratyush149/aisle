package org.aisle.application.model.response.main

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName
import javax.annotation.Generated


@Generated("jsonschema2pojo")
data class Profile__1(
    @SerializedName("first_name")
    @Expose
    var firstName: String? = null,

    @SerializedName("avatar")
    @Expose
    var avatar: String? = null
)